import { Component } from '@angular/core'
import { CoreConfig } from '@nextlevel/domain'

@Component({
  template: ``
})
export class BaseComponent {
  public debug: boolean
  // debug = !this.config.production

  constructor(config: CoreConfig) {
    // Enables debug mode to display debug data, not in production
    this.debug = !config.production
  }
}
