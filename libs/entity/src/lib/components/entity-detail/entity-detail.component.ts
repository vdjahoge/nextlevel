import { Component, Input } from '@angular/core'
import { Entity } from '../../entity.model'

@Component({
  selector: 'nextlevel-entity-detail',
  templateUrl: './entity-detail.component.html',
  styleUrls: ['./entity-detail.component.css']
})
export class EntityDetailComponent {
  title = 'Info'
  @Input() entity: Entity | undefined

  constructor() {}
}
