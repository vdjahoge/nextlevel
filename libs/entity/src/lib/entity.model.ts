// import { UserInterface } from '../../users/models'
import { User } from '@nextlevel/domain'

/**
 * Base class for all entities that are part of communication to/from services.
 */
interface EntityInterface {
  readonly id: number
  readonly createdAt: Date
  readonly createdBy: User | undefined
  readonly updatedAt: Date | undefined
  readonly lastUpdatedBy: User | undefined
}

export abstract class Entity implements EntityInterface {
  readonly id!: number
  readonly createdAt: Date
  readonly createdBy: User | undefined
  readonly updatedAt: Date | undefined
  readonly lastUpdatedBy: User | undefined

  constructor(values: EntityInterface) {
    this.id = values.id
    this.createdAt = values.createdAt || undefined
    this.createdBy = values.createdBy || undefined
    this.updatedAt = values.updatedAt || undefined
    this.lastUpdatedBy = values.lastUpdatedBy || undefined
  }
}

/**
 * Interface defining functions for (de)serializing objects from/to JSON
 */
export interface Serializer<T extends Entity> {
  fromJson(json: any): T
  toJson(resource: T): any
}

/**
 * Generic serializer immplementation class.
 */
export class EntitySerializer<T extends Entity> implements Serializer<T> {
  fromJson(json: any): T {
    return JSON.parse(json)
  }

  toJson(resource: T): any {
    return JSON.stringify(resource)
  }
}
