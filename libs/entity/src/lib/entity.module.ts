import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { BaseComponent, EntityDetailComponent } from './components'
import { HttpClientModule } from '@angular/common/http'

@NgModule({
  declarations: [BaseComponent, EntityDetailComponent],
  imports: [CommonModule, HttpClientModule],
  exports: [EntityDetailComponent, BaseComponent],
  providers: []
})
export class EntityModule {}
