import { Entity } from './entity.model'
import { Observable, throwError } from 'rxjs'
import { HttpParams, HttpClient, HttpErrorResponse } from '@angular/common/http'
import { map, catchError } from 'rxjs/operators'
import { Injectable } from '@angular/core'
import { Error } from '@nextlevel/domain'

/**
 * Generic service class for communicating objects to/from services.
 * Serves generic CRUD operations.
 */
@Injectable({
  providedIn: 'root'
})
export abstract class EntityService<T extends Entity> {
  /**
   * Service constructor.
   *
   * @param http Dependency injection http service
   * @param url The URL to communicate to.
   * @param endpoint The endpoint on the URL.
   * @param serializer The object that serializes the data.
   */
  constructor(
    public readonly http: HttpClient,
    public readonly url: string,
    public readonly endpoint: string // private readonly serializer?: Serializer<T>
  ) {}

  /**
   * Get all items.
   *
   * @params params
   */
  public list(params?: HttpParams): Observable<T[] | null> {
    const endpoint = `${this.url}${this.endpoint}`
    console.log(`list ${endpoint} params = ${params}`)
    return this.http
      .get<T[]>(endpoint, { params, observe: 'response' })
      .pipe(
        map((response) => response.body),
        catchError(this.handleError)
      )
  }

  /**
   * Create the item at the service.
   *
   * @param item Item to be created.
   */
  public create(item: T, params?: HttpParams): Observable<T> {
    const endpoint = `${this.url}${this.endpoint}`
    // console.log(`create ${endpoint}`)
    return this.http.post(endpoint, item, { params, observe: 'response' }).pipe(
      map((response) => response.body as T),
      catchError(this.handleError)
    )
  }

  /**
   * Get a single item from the service.
   *
   * @param id ID of the item to get.
   */
  public read(id: number, params?: HttpParams): Observable<T> {
    const endpoint = `${this.url}${this.endpoint}/${id}`
    // console.log(`read ${endpoint}`)
    return this.http.get(endpoint, { params, observe: 'response' }).pipe(
      map((response) => response.body as T),
      catchError(this.handleError)
    )
  }

  /**
   * Update (put) new info.
   *
   * @param item The new item.
   */
  public update(item: T, params?: HttpParams): Observable<T> {
    const endpoint = `${this.url}${this.endpoint}/${item.id}`
    // console.log(`update ${endpoint}`)
    return this.http.put(endpoint, item, { params, observe: 'response' }).pipe(
      map((response) => response.body as T),
      catchError(this.handleError)
    )
  }

  /**
   * Delete an item at the service.
   *
   * @param id ID of item to be deleted.
   */
  public delete(id: number, params?: HttpParams): Observable<T> {
    const endpoint = `${this.url}${this.endpoint}/${id}`
    // console.log(`delete ${endpoint}`)
    return this.http.delete(endpoint, { params, observe: 'response' }).pipe(
      map((response) => response.body as T),
      catchError(this.handleError)
    )
  }

  /**
   * Handle errors.
   *
   * @param error
   */
  public handleError(error: HttpErrorResponse) {
    let errorMsg
    if (error && error.error instanceof ErrorEvent) {
      errorMsg = `An error occurred: ${error.error.message}`
    } else if (error && error.status) {
      errorMsg = `Backend returned code ${error.status} - ${error.statusText}`
    } else {
      errorMsg = error.toString()
    }
    console.log('EntityService.handleError', errorMsg)

    // return an error observable with a user-facing error message
    return throwError(new Error(errorMsg))
  }
}
