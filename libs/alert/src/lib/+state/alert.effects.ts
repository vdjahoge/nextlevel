import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType, Effect } from '@ngrx/effects'
import { catchError, map, switchMap, tap } from 'rxjs/operators'
import { AlertActionTypes } from './alert.actions'
import * as fromAlertActions from './alert.actions'
import { AlertType } from './alert.models'
import { ToastrService } from 'ngx-toastr'
import { AlertEntity } from './alert.models'
import { of } from 'rxjs'

@Injectable()
export class AlertEffects {
  constructor(private readonly actions$: Actions, private readonly toastr: ToastrService) {}

  @Effect({ dispatch: true })
  showAlert$ = this.actions$.pipe(
    ofType(AlertActionTypes.SHOW_ALERT),
    map((action: fromAlertActions.ShowAlert) => action.payload),
    tap((alertInfo: AlertEntity) => {
      switch (alertInfo.type.toString()) {
        case AlertType.SUCCESS:
          // this.toastr.success(
          //   `<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">${alertInfo.message}</span>`,
          //   alertInfo.title
          // )

          this.toastr.success(
            '<span data-notify="icon" class="nc-icon nc-bell-55"></span><span data-notify="message">' +
              alertInfo.message +
              '</span>',
            alertInfo.title,
            {
              timeOut: 14000,
              closeButton: true,
              enableHtml: true,
              toastClass: 'alert alert-success alert-with-icon',
              positionClass: 'toast-bottom-left'
            }
          )

          break

        case AlertType.WARNING:
          this.toastr.warning(alertInfo.message, alertInfo.title)
          break
        case AlertType.ERROR:
          this.toastr.error(alertInfo.message, alertInfo.title)
          break
        case AlertType.INFO:
          this.toastr.info(alertInfo.message, alertInfo.title)
          break
        default:
          this.toastr.info(alertInfo.message, alertInfo.title)
      }
    }),
    map(() => new fromAlertActions.ShowAlertDone()),
    catchError((error) => of(new fromAlertActions.ShowAlertFailed()))
  )
}
