import { Action } from '@ngrx/store'
import { AlertEntity } from './alert.models'

export enum AlertActionTypes {
  SHOW_ALERT = '[Alert] Show Alert',
  SHOW_ALERT_DONE = '[Alert] Show Alert Done',
  SHOW_ALERT_FAILED = '[Alert] Show Alert Failed'
}

export class ShowAlert implements Action {
  readonly type = AlertActionTypes.SHOW_ALERT
  constructor(public payload: AlertEntity) {}
}

export class ShowAlertDone implements Action {
  readonly type = AlertActionTypes.SHOW_ALERT_DONE
  constructor() {}
}

export class ShowAlertFailed implements Action {
  readonly type = AlertActionTypes.SHOW_ALERT_FAILED
  constructor() {}
}

export type AlertActions = ShowAlert | ShowAlertFailed | ShowAlertDone
