/**
 * Interface for the 'Alert' data
 */

export const enum AlertType {
  INFO = 'Info',
  SUCCESS = 'Success',
  WARNING = 'Warning',
  ERROR = 'Error'
}

export interface AlertEntity {
  title: string | undefined
  message: string | undefined
  type: AlertType | null
}
