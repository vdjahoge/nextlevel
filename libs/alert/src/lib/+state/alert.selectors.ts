import { createFeatureSelector, createSelector } from '@ngrx/store'
import { ALERT_FEATURE_KEY, AlertState, alertAdapter } from './alert.reducer'

// Lookup the 'Alert' feature state managed by NgRx
export const getAlertState = createFeatureSelector<AlertState>(ALERT_FEATURE_KEY)

const { selectAll, selectEntities } = alertAdapter.getSelectors()

export const getAlertTitle = createSelector(
  getAlertState,
  (state: AlertState) => state[ALERT_FEATURE_KEY].title
)

export const getAlertMessage = createSelector(
  getAlertState,
  (state: AlertState) => state[ALERT_FEATURE_KEY].message
)

export const getAlertType = createSelector(
  getAlertState,
  (state: AlertState) => state[ALERT_FEATURE_KEY].type
)

// export const getSelected = createSelector(
//   getAlertEntities,
//   getSelectedId,
//   (entities, selectedId) => selectedId && entities[selectedId]
// )
