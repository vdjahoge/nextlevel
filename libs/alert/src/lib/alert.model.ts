/**
 * Baseclass that models the information in a popup Toast alert.
 */
export class AlertInfo {
  constructor(public title: string, public message: string) {}
}
