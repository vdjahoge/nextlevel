import { LoginComponent } from './login/login.component'
import { RegisterComponent } from './register/register.component'
import { UserDetailComponent } from './user-detail/user-detail.component'
import { UserListComponent } from './user-list/user-list.component'
import { UserEditComponent } from './user-edit/user-edit.component'

export const containers: any[] = [
  LoginComponent,
  RegisterComponent,
  UserDetailComponent,
  UserListComponent,
  UserEditComponent
]

export * from './login/login.component'
export * from './register/register.component'
export * from './user-detail/user-detail.component'
export * from './user-list/user-list.component'
export * from './user-edit/user-edit.component'
