import { Injectable } from '@angular/core'
import { Observable, of, EMPTY, throwError, BehaviorSubject } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { tap, catchError, map } from 'rxjs/operators'
import { EntityService } from '@nextlevel/entity'
import { CoreConfig } from '@nextlevel/domain'
import { Authenticate, User } from '@nextlevel/domain'

@Injectable({
  providedIn: 'root'
})
export class AuthService extends EntityService<any> {
  // Logging classname tag
  static readonly TAG = AuthService.name

  // store the URL so we can redirect after logging in
  public readonly redirectUrl = '/dashboard'

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json'
  })

  constructor(http: HttpClient, private config: CoreConfig) {
    super(http, config.API_URL, 'users')
  }

  private userSubject$ = new BehaviorSubject<User>(null)
  user$ = this.userSubject$.asObservable()
  // __constructor(private httpClient: HttpClient) {
  //   const user = localStorage.getItem('user')
  //   if (user) {
  //     this.userSubject$.next(JSON.parse(user))
  //   }
  // }

  __login(authenticate: Authenticate): Observable<User> {
    const API_LOGIN = this.config.API_URL + '/login'
    return this.http.post<User>(API_LOGIN, authenticate).pipe(
      tap((user: User) => {
        this.userSubject$.next(user)
        localStorage.setItem('user', JSON.stringify(user))
      })
    )
  }

  login(email: string, password: string): Observable<any> {
    // this.logger.debug(AuthService.TAG, 'login')
    const API_LOGIN = this.config.API_URL + '/login'
    // this.logger.debug(AuthService.TAG, 'login via hardcoded URL ', API_LOGIN)
    return this.http.post(API_LOGIN, { email, password }, { headers: this.headers }).pipe(
      map((result) => result as any),
      catchError(this.handleError)
    )
  }

  public logout(): Observable<boolean> {
    // this.logger.debug(AuthService.TAG, 'logout - remove local user')
    localStorage.removeItem('userName')
    localStorage.removeItem('userRoles')
    localStorage.removeItem('token')
    return of(true)
  }

  public getUserFromLocalStorage(): Observable<any> {
    console.log('getuser from storage')
    const token = localStorage.getItem('token')
    if (!token || token === null) {
      return throwError({ message: 'No user in local storage' })
    }

    const userName = localStorage.getItem('userName')
    const userRoles = localStorage.getItem('userRoles')

    const storedUser: any = {
      userName,
      userRoles: [],
      token
    }

    return of(storedUser)
  }

  public saveUserToLocalStorage(user: any): Observable<boolean> {
    localStorage.setItem('userName', 'TESTSTRING!') // user.userName)
    localStorage.setItem('token', 'TESTSTRING!') // user.token)
    // localStorage.setItem('userRoles', user.userRoles)
    return of(true)
  }
}
