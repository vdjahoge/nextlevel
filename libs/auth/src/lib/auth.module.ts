import { ModuleWithProviders, NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Route } from '@angular/router'

import * as fromComponents from './components'
import * as fromContainers from './containers'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'
import * as fromAuth from './+state/auth.reducer'
import { AuthEffects } from './+state/auth.effects'
import { HttpClientModule } from '@angular/common/http'
import { AUTH_FEATURE_KEY } from './+state/auth.reducer'
import { CoreConfig } from '@nextlevel/domain'
import { AlertModule } from '@nextlevel/alert'

export const authRoutes: Route[] = [
  { path: 'login', component: fromContainers.LoginComponent },
  { path: 'register', component: fromContainers.RegisterComponent },
  { path: 'profile', component: fromContainers.UserDetailComponent }
]

@NgModule({
  declarations: [...fromComponents.components, ...fromContainers.containers],
  imports: [
    CommonModule,
    RouterModule.forRoot(authRoutes),
    HttpClientModule,
    AlertModule,
    StoreModule.forFeature(AUTH_FEATURE_KEY, fromAuth.authReducer, { initialState: fromAuth.initialState }),
    EffectsModule.forFeature([AuthEffects])
  ],
  providers: [AuthEffects]
})
export class AuthModule {
  static forRoot(config: CoreConfig | null | undefined): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [
        { provide: CoreConfig, useValue: config || {} }
        // { provide: RouterStateSerializer, useClass: CustomSerializer }
      ]
    }
  }
}
