import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Authenticate, Error, User } from '@nextlevel/domain'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Observable, of } from 'rxjs'
import { tap, map, catchError, switchMap } from 'rxjs/operators'
import { AuthService } from './../auth.service'
import * as fromAuthActions from './auth.actions'
import * as fromAlert from '@nextlevel/alert'

@Injectable()
export class AuthEffects {
  constructor(
    private readonly actions: Actions,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  @Effect()
  isLoggedIn$ = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.IS_LOGGEDIN),
    switchMap(() => this.authService.getUserFromLocalStorage()),
    map((user: User) => new fromAuthActions.IsLoggedInSuccess(user)),
    catchError((error) => of(new fromAuthActions.IsLoggedInFail(error)))
  )

  @Effect({ dispatch: false })
  LoggedInSuccess: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.IS_LOGGEDIN_SUCCESS),
    map((action: fromAuthActions.IsLoggedInSuccess) => action.payload)
  )

  @Effect()
  Login: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGIN),
    map((action: fromAuthActions.Login) => action.payload),
    switchMap((payload: Authenticate) => {
      return this.authService.login(payload.email, payload.password).pipe(
        map((user: User) => {
          return new fromAuthActions.LoginSuccess(user)
        }),
        catchError((error: Error) => {
          return of(new fromAuthActions.LoginFailed(error))
        })
      )
    })
  )

  @Effect()
  LoginSuccess: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGIN_SUCCESS),
    map((action: fromAuthActions.LoginSuccess) => action.payload),
    map((user: User) => this.authService.saveUserToLocalStorage(user)),
    map(
      () =>
        new fromAlert.ShowAlert({
          title: 'LoggedIn',
          message: 'You are logged in',
          type: fromAlert.AlertType.SUCCESS
        })
    ),
    tap(() => this.router.navigate(['/']))
  )

  @Effect()
  LoginFailed: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGIN_FAILED),
    map((action: fromAuthActions.LoginFailed) => action.payload),
    map(
      (error: Error) =>
        new fromAlert.ShowAlert({
          title: 'NOT LoggedIn',
          message: error.message,
          type: fromAlert.AlertType.ERROR
        })
    )
  )

  @Effect()
  LogOut: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGOUT),
    switchMap(() => this.authService.logout()),
    map(() => new fromAuthActions.LogOutSuccess()),
    catchError((error) => of(new fromAuthActions.LogOutFailed()))
  )

  @Effect({ dispatch: false })
  LogOutSuccess: Observable<any> = this.actions.pipe(
    ofType(fromAuthActions.AuthActionTypes.LOGOUT_SUCCESS, fromAuthActions.AuthActionTypes.LOGOUT_FAILED),
    tap(() => this.router.navigate(['/']))
  )
}
