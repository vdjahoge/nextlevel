import { User } from '@nextlevel/domain'

/**
 * Interface for the 'Auth' data
 */
export interface AuthEntity {
  id: string | number // Primary ID

  loading: boolean
  loaded: boolean
  user: User
  error: ''
}
