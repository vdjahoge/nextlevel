import { Error } from '@nextlevel/domain'
import { LoginFailed } from './auth.actions'
import { authReducer, initialState } from './auth.reducer'

describe('authReducer', () => {
  it('should work', () => {
    const action: LoginFailed = new LoginFailed(new Error('some message'))
    const actual = authReducer(initialState, action)
    const expected = {
      error: '',
      id: 0,
      loaded: false,
      loading: false,
      user: null
    }
    expect(actual).toEqual(expected)
  })
})
