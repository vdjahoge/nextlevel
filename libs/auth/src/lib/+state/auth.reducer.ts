import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity'
import { AuthActions, AuthActionTypes } from './auth.actions'
import { AuthEntity } from './auth.models'
import { User } from '@nextlevel/domain'

export const AUTH_FEATURE_KEY = 'auth'

// export interface AuthData {
//   loading: boolean
//   loaded: boolean
//   user: User
//   error: ''
// }

// export interface AuthState {
//   // readonly auth: AuthData
//   readonly auth: AuthEntity
// }

export interface AuthState {
  readonly [AUTH_FEATURE_KEY]: AuthEntity
}

export const initialState: AuthEntity = {
  id: 0,
  error: '',
  user: null,
  loaded: false,
  loading: false
}

export const authAdapter: EntityAdapter<AuthEntity> = createEntityAdapter<AuthEntity>()

export function authReducer(state = initialState, action: AuthActions): AuthEntity {
  switch (action.type) {
    //
    //
    case AuthActionTypes.LOGIN:
      return { ...state, loading: true }
    //
    //
    case AuthActionTypes.LOGIN_SUCCESS: {
      return { ...state, user: action.payload, loading: false, loaded: true }
    }
    //
    //
    case AuthActionTypes.LOGIN_FAILED: {
      return { ...state, user: null, loading: false }
    }
    //
    //
    default:
      return state
  }
}
