import { User } from '@nextlevel/domain'
import { AuthEntity } from './auth.models'
import { AuthState, authAdapter, initialState } from './auth.reducer'
import * as AuthSelectors from './auth.selectors'

describe.skip('Auth Selectors', () => {
  const ERROR_MSG = 'No Error Available'
  const getAuthId = (it) => it['id']
  const createAuthEntity = (
    id: string = '',
    loading: boolean = false,
    loaded: boolean = false,
    user: User = undefined,
    error: string = ''
  ) =>
    ({
      id,
      loading,
      loaded,
      user,
      error
      // name: name || `name-${id}`
    } as AuthEntity)

  let state

  beforeEach(() => {
    state = {
      // auth: authAdapter.setAll(
      //   [createAuthEntity('PRODUCT-AAA'), createAuthEntity('PRODUCT-BBB'), createAuthEntity('PRODUCT-CCC')],
      //   {
      //     ...initialState,
      //     // selectedId: 'PRODUCT-BBB',
      //     loading: false,
      //     loaded: false,
      //     user: undefined,
      //     error: ERROR_MSG
      //   }
      // )
    }
  })

  describe.skip('Auth Selectors', () => {
    it('getAllAuth() should return the list of Auth', () => {
      const results = AuthSelectors.getAllAuth(state)
      const selId = getAuthId(results[1])

      // expect(results.length).toBe(3)
      expect(selId).toBe('PRODUCT-BBB')
    })

    it('getSelected() should return the selected Entity', () => {
      const result = AuthSelectors.getAllAuth(state)
      const selId = getAuthId(result)

      expect(selId).toBe('PRODUCT-BBB')
    })

    it("getAuthLoaded() should return the current 'loaded' status", () => {
      const result = AuthSelectors.getAuthLoaded(state)

      expect(result).toBe(true)
    })

    it("getAuthError() should return the current 'error' state", () => {
      const result = AuthSelectors.getAuthError(state)

      expect(result).toBe(ERROR_MSG)
    })
  })
})
