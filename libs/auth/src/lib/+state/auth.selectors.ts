import { createFeatureSelector, createSelector } from '@ngrx/store'
import { AUTH_FEATURE_KEY, AuthState, authAdapter } from './auth.reducer'

// Lookup the 'Auth' feature state managed by NgRx
// export const getAuthState = createFeatureSelector<AuthPartialState, State>(AUTH_FEATURE_KEY)

export const getAuthState = createFeatureSelector<AuthState>(AUTH_FEATURE_KEY)
// export const getAuthState = createFeatureSelector<AuthData>('auth');
//   fromAppReducer.getAppState,
//   (state: fromAppReducer.ApplicationState) => state.courses
// )

const { selectAll, selectEntities } = authAdapter.getSelectors()

export const getAuthLoaded = createSelector(getAuthState, (state: AuthState) => state.auth.loaded)

export const getAuthUser = createSelector(getAuthState, (state) => state.auth.user)

export const getAuthError = createSelector(getAuthState, (state: AuthState) => state.auth.error)

export const getAllAuth = createSelector(getAuthState, (state: AuthState) => state.auth)

// export const getSelectedId = createSelector(getAuthState, (state: AuthState) => state.auth.selectedId)

// export const getAuthEntities = createSelector(getAuthState, (state: AuthState) => selectEntities(state.auth))

// export const getSelected = createSelector(
//   getAuthEntities,
//   getSelectedId,
//   (entities, selectedId) => selectedId && entities[selectedId]
// )
