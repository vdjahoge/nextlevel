import { LoginFormComponent } from './login-form/login-form.component'
import { RegisterFormComponent } from './register-form/register-form.component'
import { UserFormComponent } from './user-form/user-form.component'

export const components: any[] = [LoginFormComponent, RegisterFormComponent, UserFormComponent]

export * from './login-form/login-form.component'
export * from './register-form/register-form.component'
export * from './user-form/user-form.component'
