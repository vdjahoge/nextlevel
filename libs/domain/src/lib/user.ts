export interface User {
  id: number

  firstname: string
  middlename: string
  lastname: string
  email: string

  role: string

  country: string
  token: string
}
