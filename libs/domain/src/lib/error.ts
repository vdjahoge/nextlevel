export class Error {
  constructor(private _message: string, private _errorCode?: number) {}

  get message() {
    return this._message
  }

  get errorCode() {
    return this._errorCode
  }
}
