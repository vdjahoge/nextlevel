export class CoreConfig {
  readonly logConfig: any

  // Base URL of backend used by EntityService
  readonly API_URL: string

  // When not in production, BaseComponent is debug-enabled.
  readonly production: boolean
}
