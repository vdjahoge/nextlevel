import { Routes } from '@angular/router'
import { DashboardComponent } from './pages/dashboard/dashboard.component'

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  { path: 'dashboard', pathMatch: 'full', component: DashboardComponent },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
]
