import { Component } from '@angular/core'

@Component({
  selector: 'nextlevel-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'courses-admin-portal'
}
