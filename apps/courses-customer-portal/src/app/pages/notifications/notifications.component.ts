import { Component } from '@angular/core'
import { AlertState, AlertType, ShowAlert } from '@nextlevel/alert'
import { Store } from '@ngrx/store'

@Component({
  selector: 'notifications-cmp',
  templateUrl: 'notifications.component.html'
})
export class NotificationsComponent {
  constructor(private store: Store<AlertState>) {}

  showNotification(from, align) {
    this.store.dispatch(
      new ShowAlert({ title: 'Toppie!', message: 'Alles is goed gegaan', type: AlertType.SUCCESS })
    )
  }
}
