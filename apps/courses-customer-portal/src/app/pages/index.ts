import { DashboardComponent } from './dashboard/dashboard.component'
import { UserComponent } from './user/user.component'
import { TableComponent } from './table/table.component'
import { TypographyComponent } from './typography/typography.component'
import { NotificationsComponent } from './notifications/notifications.component'
import { UpgradeComponent } from './upgrade/upgrade.component'

export const components: any[] = [
  DashboardComponent,
  UserComponent,
  TableComponent,
  TypographyComponent,
  NotificationsComponent,
  UpgradeComponent
]

export * from './dashboard/dashboard.component'
export * from './notifications/notifications.component'
export * from './table/table.component'
export * from './typography/typography.component'
export * from './upgrade/upgrade.component'
export * from './user/user.component'
