import { TestBed, async, ComponentFixture } from '@angular/core/testing'
import { AppComponent } from './app.component'
import { Component, Directive, HostListener, Input } from '@angular/core'
import { Router } from '@angular/router'
import { AuthModule } from '@nextlevel/auth'

//
// Since the app.component.html template uses some component selectors,
// we need to stub these in the test.
//
@Component({ selector: 'custportal-dashboard', template: '' })
class DashboardStubComponent {}

@Component({ selector: 'custportal-navbar', template: '' })
class NavbarStubComponent {
  @Input() title: string
}

@Component({ selector: 'custportal-footer', template: '' })
class FooterStubComponent {}

@Component({ selector: 'custportal-searchbar', template: '' })
class SearchbarStubComponent {}

// tslint:disable-next-line: component-selector
@Component({ selector: 'router-outlet', template: '' })
class RouterOutletStubComponent {}

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[routerLink]'
})
class RouterLinkStubDirective {
  // tslint:disable-next-line: no-input-rename
  @Input('routerLink') linkParams: any
  navigatedTo: any = null

  @HostListener('click')
  onClick() {
    this.navigatedTo = this.linkParams
  }
}

//
// The test suite for AppComponent.
//
describe.skip('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>
  let app: AppComponent
  let routerSpy
  let authServiceSpy // : { userIsLoggedIn: jasmine.Spy }

  beforeEach(async(() => {
    //
    // Dit werkt nog niet: jasmine functies omzetten naar Jest functies.
    // https://www.xfive.co/blog/testing-angular-faster-jest/
    //
    // routerSpy = jest.fn({key: jest.fn()})
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])
    authServiceSpy = jasmine.createSpyObj('authService', ['login', 'userIsLoggedIn'])

    TestBed.configureTestingModule({
      declarations: [
        // The 'real' component that we will test
        AppComponent,

        // Required stubbed components
        // DashboardStubComponent,
        // NavbarStubComponent,
        // FooterStubComponent,
        // SearchbarStubComponent,
        RouterOutletStubComponent,
        RouterLinkStubDirective
      ],
      imports: [AuthModule],
      // Don't provide the real service! Provide a test-double instead!
      providers: [
        { provide: Router, useValue: routerSpy }
        // { provide: AuthService, useValue: authServiceSpy }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(AppComponent)
    app = fixture.debugElement.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create the app', () => {
    fixture.detectChanges()
    expect(app).toBeTruthy()
  })

  it.skip(`should have as title 'courses-customer-portal'`, () => {
    fixture.detectChanges()
    expect(app.title).toEqual('courses-customer-portal')
  })

  // it('should render title', () => {
  //   fixture = TestBed.createComponent(AppComponent)
  //   fixture.detectChanges()
  //   const compiled = fixture.nativeElement
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to courses-customer-portal!')
  // })
})
