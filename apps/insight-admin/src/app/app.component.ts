import { Component } from '@angular/core'

@Component({
  selector: 'insight-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'insight-admin'
}
