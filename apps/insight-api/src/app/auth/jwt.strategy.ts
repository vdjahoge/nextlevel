import { Injectable } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { ConfigService } from '@nestjs/config'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { UserService } from './user/user.service'
import { environment } from '../../environments/environment'
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly configService: ConfigService, private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: configService.get('JWT_IGNORE_EXPIRATION') === 'true',
      secretOrKey: environment.JWT_SECRET
    })
  }
  async validate(payload: any) {
    return this.userService.find(payload.email)
  }
}
