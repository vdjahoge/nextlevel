export const environment = {
  production: true,

  scheme: 'neo4j',
  host: 'localhost',
  port: 7687,
  username: 'neo4j',
  password: 'neo',

  HASH_ROUNDS: 10,

  JWT_SECRET: 'mySecret',
  JWT_EXPIRES_IN: '30d',

  AUTH_PROPERTY: 'email'
}
